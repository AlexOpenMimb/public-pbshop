import { showPasw } from "./events/login.js";
import hamburguerMenu, { subMenu } from "./events/menu.js";

const d = document;

d.addEventListener("DOMContentLoaded", (e) => {
  hamburguerMenu("#menu-btn", ".menu");
  subMenu();
  showPasw("#btn-eye");

});





const d = document,
  $ul = d.querySelectorAll("ul");

export default function hamburguerMenu(buttonMenu, menu) {
  d.addEventListener("click", (e) => {
    if (e.target.matches(`${buttonMenu} *`)) {
      e.path[1].firstElementChild.classList.toggle("d-none");
      e.path[1].lastElementChild.classList.toggle("d-none");

      d.querySelector(menu).classList.toggle("menu--active");
      $ul.forEach((el) => {
        if (el.className === "sub-menu--active") {
          el.classList.remove("sub-menu--active");
          el.classList.add("sub-menu--unactive");
        }
      });
    }
  });
}

export function subMenu() {
  d.addEventListener("click", (e) => {
      
      if (window.innerWidth < 1200) {
        if (e.target.matches(".link_div")) {
          
          let sibling = e.path[1].nextElementSibling;

        if(sibling === null){
         return
       }else{

        let  menuActive = sibling.className,
       handleMenu = sibling.classList;

        expandMenu(menuActive,handleMenu,e)
        
       } 
     }


      if(e.target.matches(".link_a")){
         
        let sibling = e.path[0].nextElementSibling; 

          if(sibling === null){
           return
         }else{

          let  menuActive = sibling.className,
         handleMenu = sibling.classList;
         
           expandMenu(menuActive,handleMenu,e)
         } 
        }


        if(e.target.matches(".link_img")){

          let sibling = e.path[1].nextElementSibling; 

          if(sibling === null){
           return
         }else{

          let  menuActive = sibling.className,
         handleMenu = sibling.classList;
        
         expandMenu(menuActive,handleMenu,e)
          
         } 
        }
      
    }
  });
}

function expandMenu (menu,handM,even){
  even.preventDefault();
  if (menu === "sub-menu--unactive") {
    handM.remove("sub-menu--unactive");
    handM.add("sub-menu--active");
  } else {
    handM.remove("sub-menu--active");
    handM.add("sub-menu--unactive");
  }
}